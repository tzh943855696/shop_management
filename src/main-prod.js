import Vue from 'vue'
import App from './App.vue'
import router from './router'
import axios from 'axios'
// import './plugins/element.js'
import './assets/css/global.css'
import './assets/fonts/iconfont.css'
import TreeTable from 'vue-table-with-tree-grid'
import VueQuillEditor from 'vue-quill-editor'

// import css
// import 'quill/dist/quill.core.css'
// import 'quill/dist/quill.snow.css'
// import 'quill/dist/quill.bubble.css'

//导入Nprogress
import NProgress from 'nprogress'
// import 'nprogress/nprogress.css'

axios.defaults.baseURL = `http://47.107.95.4:8888/api/private/v1/`
axios.interceptors.request.use(config => {
  //展示进度条
  NProgress.start()
  config.headers.Authorization = window.sessionStorage.getItem('token')
  return config
})


axios.interceptors.response.use(config => {
  //关闭进度条
  NProgress.done()
  return config
})

Vue.config.productionTip = false
Vue.prototype.$axios = axios

Vue.component('tree-table', TreeTable)
Vue.use(VueQuillEditor)
Vue.filter('dateFormat',function(oldVal) {
  const date = new Date(oldVal)
  const year = date.getFullYear()
  const month = (date.getMonth() + 1 + '').padStart(2,'0')
  const day = (date.getDate() + '').padStart(2,'0')

  const hour = (date.getHours() + '').padStart(2,'0')
  const minute = (date.getMinutes() + '').padStart(2,'0')
  const second = (date.getSeconds() + '').padStart(2,'0')

  return `${year}-${month}-${day}   ${hour}:${minute}:${second}`
})




new Vue({
  router,
  render: h => h(App)
}).$mount('#app')